# TypeScript REST Library for Wowza Streaming Engine
A wrapper for the Wowza Streaming Engine [media server software](https://www.wowza.com/products/streaming-engine) REST API.