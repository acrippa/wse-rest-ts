import { Wowza } from "./wowza";

export interface StreamTarget extends Wowza {
  config: StreamTargetConfig
}

export interface StreamTargetConfig {
  sourceStreamName: string
  entryName: string
  profile: string
  host: string
  application: string
  userName: string
  password: string
  streamName: string
  appName: string
}