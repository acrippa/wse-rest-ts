import { Wowza } from "./wowza"

export interface Application extends Wowza {
  config: ApplicationConfig
}

export interface ApplicationConfig {
  appType: string
  name: string
  clientStreamReadAccess: string
  clientStreamWriteAccess: string
  description: string
}