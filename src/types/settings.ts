export interface Settings {
  debug: boolean,
  host: string,
  serverInstance: string,
  vhostInstance: string,
  username: string,
  password: string,
  useDigest: boolean
}
