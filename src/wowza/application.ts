import { Application as ApplicationInterface, ApplicationConfig, Settings } from 'types'
import { Wowza } from './wowza'

export class Application extends Wowza implements ApplicationInterface {
  readonly config: ApplicationConfig = {
    name: 'live',
    appType: 'Live',
    clientStreamReadAccess: '*',
    clientStreamWriteAccess: '*',
    description: '*'
  }

  private constructor(settings: Settings, config: ApplicationConfig) {
    super(settings)
    this.config = config
  }

  static getRestURI = (application: Application) => {

  }

}