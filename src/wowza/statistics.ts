import { Application, Server } from 'types'
import { Wowza } from "./wowza"
import { Client } from 'utils'

export class Statistics extends Wowza {
  static getApplicationStatistics = (application: Application) => { 
    Application.getRestURI(application)

    return $this -> sendRequest($this -> preparePropertiesForRequest(self:: class), [], self:: VERB_GET);
  }

  static getApplicationStatisticsHistory = () => { }


  static getIncomingApplicationStatistics = () => { }

  static getServerStatistics = (server: Server) => { }

  static getServerStatisticsCurrent = (server: Server) => { }
}