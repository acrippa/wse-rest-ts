import { Settings, Wowza as WowzaInterface } from '../types'

export class Wowza implements WowzaInterface {
  settings: Settings;
  protected constructor(settings: Settings = {
    debug: false,
    host: 'http://localhost:8087/v2',
    serverInstance: '_defaultServer_',
    vhostInstance: '_defaultVHost_',
    username: '',
    password: '',
    useDigest: true
  }) {
    this.settings = settings
  }
}