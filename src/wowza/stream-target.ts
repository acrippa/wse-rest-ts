import { StreamTarget as StreamTargetInterface, Settings, StreamTargetConfig } from '../types'
import { Wowza } from "./wowza"
import { Client } from 'utils'

export class StreamTarget extends Wowza implements StreamTargetInterface {
  readonly config: StreamTargetConfig = {
    sourceStreamName: 'myStream',
    entryName: 'ppsource',
    profile: 'rtmp',
    host: 'localhost',
    application: 'live',
    userName: '',
    password: '',
    streamName: 'myStream',
    appName: '',
  }

  private constructor(settings: Settings, config: StreamTargetConfig) {
    super(settings)
    this.config = config
  }

  static async create(settings: Settings, config: StreamTargetConfig) {
    const st = new StreamTarget(settings, config)
    await Client.post(`${StreamTarget.restURI(st)}/${st.config.entryName}`,
      st.config)
  }

  static async getAll(st: StreamTarget) {
    const uri = `${StreamTarget.restURI(st)}/${st.config.entryName}`
    await Client.get(uri)
  }

  static async remove(st: StreamTarget) {
    await Client.delete(`${StreamTarget.restURI(st)}/${st.config.entryName}`)
  }

  static restURI = (st: StreamTarget) => {
    return `${st.settings.host}/servers/${st.settings.serverInstance}/vhosts/${st.settings.vhostInstance}/applications/${st.config.appName}/pushpublish/mapentries`
  }

}