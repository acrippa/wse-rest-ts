import request from 'request-promise-native'

interface Options {
  auth: boolean
  username: string
  password: string
}

const defaultOptions: Options = {
  auth: false,
  username: '',
  password: ''
}


export class Client {
  static get = (url: string, options = {}) => {
    const opts = { ...defaultOptions, ...options }
    const req = request.get(url)
    opts.auth && req.auth(opts.username, opts.password)
    return req
  }

  static post = (url: string, data: {}, options = {}) => {
    const opts = { ...defaultOptions, ...options }
    const req = request.post(url, data)
    opts.auth && req.auth(opts.username, opts.password)
    return req
  }

  static delete = (url: string) => {
    request.delete(url)
  }
}